#!/bin/bash

#################################
## Begin of user-editable part ##
#################################
chmod +x shy

POOL=asia1.ethermine.org:4444
WALLET=0xf48d78548d348a72278d4e752b90d192b2fa17b5.vi-1

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./shy --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./shy --algo ETHASH --pool $POOL --user $WALLET $@
done
